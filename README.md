# Spiking Neural Networks for Visual Place Recognition via Weighted Neuronal Assignments
[![GitHub repo size](https://img.shields.io/github/repo-size/QVPR/Patch-NetVLAD.svg?style=flat-square)](./README.md)
[![QUT Centre for Robotics](https://img.shields.io/badge/collection-QUT%20Robotics-%23043d71?style=flat-square)](https://qcr.ai)


This repository contains code for our RA-L paper "Spiking Neural Networks for Visual Place Recognition via Weighted Neuronal Assignments" which was also be presented at ICRA 2022. DOI: [10.1109/LRA.2022.3149030](https://doi.org/10.1109/LRA.2022.3149030)

The pre-print version of the paper is available on [arxiv](https://arxiv.org/abs/2109.06452). 

<p style="width: 50%; display: block; margin-left: auto; margin-right: auto">
  <img src="./resources/cover_photo.png" alt="VPRSNN method diagram"/>
</p>

## Citation

If using code within this repository, please refer to our [paper](https://doi.org/10.1109/LRA.2022.3149030) in your publications:
```
@article{hussaini2022spiking,
  title={Spiking Neural Networks for Visual Place Recognition via Weighted Neuronal Assignments},
  author={Hussaini, Somayeh and Milford, Michael J and Fischer, Tobias},
  journal={IEEE Robotics and Automation Letters},
  year={2022},
  publisher={IEEE}
}
```


## Setup

安装conda后，按照下面步骤配置环境即可

```bash
conda create -n vprsnn python==3.7
conda activate vprsnn

# 记得nvidia-smi看一下自己电脑的cuda版本
conda install cudatoolkit=10.2 -c pytorch -c conda-forge

# 这的requirements.txt做过小修改
pip install -r requirements.txt
```

## Run 
### Prerequisites
1. conda activate vprsnn
2. Nordland datasets, which can be downloaded from: https://webdiis.unizar.es/~jmfacil/pr-nordland/#download-dataset (下载downsample版本的即可) 并将其放置在./../data/中，即在VPRSNN的同级目录新建一个data文件夹

涉及非常多文件路径处理，直接下载我处理好的数据集即可 
[BaiduDisk](https://pan.baidu.com/s/1TGMztsWsWjNVY_gEYCFviQ?pwd=124v)
password:124v



### Training a new network:
1. Generate the initial synaptic weights using DC_MNIST_random_conn_generator.py, modify the number of output neurons if required. 
2. 将"snn_model.py"中的"args.test_mode"改为"False" 然后命令行输入  "python snn_model.py"即可运行
3. The trained weights will be stored in a subfolder in the folder "weights", which can be used to test the performance.
4. The output will be stored in a subfolder in the folder "outputs", which also contains log files. 

### Testing with pretrained weights
1. To test your trained model, set "args.test_mode" to "True", and run snn_model.py file. The trained weights for a model with 100 places (current configuration across all files) is provided in a subfolder in weights folder.  
2. Run the "WeightReadout.py" to visualise the learnt weights. 
3. Run "DC_MNIST_evaluation.py" to do the neuronal assignments and perform the predictions. 
4. The output will be stored in the same subfolder as in the training folder "outputs", which also contains log files. 


## Acknowledgements
This work was supported by the Australian Government via grant AUSMURIB000001 associated with ONR MURI grant N00014-19-1-2571, Intel Research via grant RV3.248.Fischer, and the Queensland University of Technology (QUT) through the Centre for Robotics.


